const  MongoClient = require("mongodb").MongoClient;

let db;

async function connectToServer() {
    const client = await MongoClient.connect( "mongodb://mongo:27017/societies", { useUnifiedTopology: true, useNewUrlParser: true });
    db =  client.db("societies");
    if (db) {
      return;
    } else {
      throw new Error ("Fatal error, couldn't connect to MongoDB");
    }
}

function getDb() {
  return db;
}

module.exports = {connectToServer, getDb}

const { ObjectID } = require("bson");
const { getDb } = require("../../mongodb");

const verbosityLevel = 1;
const consoleLogDebug = (stringToConsoleLog, importanceLevel=0) => {
    if ( importanceLevel <= verbosityLevel ) { console.log( stringToConsoleLog ) }
}

async function createSocietyType( societyType ) { return new Promise(async (resolve, reject) => { 

    const societiesTypesDocuments = await getDb().collection('society_types').findOne({name: societyType})

    if(!societiesTypesDocuments) {
        getDb().collection('society_types').insertOne({name: societyType}).then( () => {   
            consoleLogDebug(`Society Type successfully added: ${societyType}`, 1)
            resolve ({'resultSocietyType':'success' })
        }).catch( error => {
            consoleLogDebug(`Society Type not successfully added: ${societyType}`)
            console.error(error)
            reject(error)
        })   
    } else{
        resolve ({'resultSocietyType':'success' })
    }
})}

async function createSociety(society){ return new Promise(async (resolve, reject) => {

    society.created_ts = Date.now()
    const entryId = ObjectID()
    society._id = entryId
    const entryIdString = ObjectID(entryId).toString()
    consoleLogDebug(entryId, 1)

    await getDb().collection('societies').insertOne(society).catch( error => {
        console.error(error);
        reject(error);
    })

    const societyType = society.societyType;
    await createSocietyType(societyType).catch( error => {
        console.error(error);
        reject(error);
    })

    consoleLogDebug(entryIdString, 1)
    resolve( entryIdString )
})}

async function getSocieties(){ return new Promise((resolve, reject) => {
    consoleLogDebug("request to get all societies", 1);
    getDb().collection('societies').find().toArray(function (err, docs) {
        for (const doc of docs){ doc.entryClass = "societies" }
        consoleLogDebug(docs, 2)
        resolve (docs);
    })
})}

async function getSocietyTypes(){ return new Promise((resolve, reject) => {
    consoleLogDebug("request to get all society types", 1);
    getDb().collection('society_types').find().toArray(function (err, docs) {
        consoleLogDebug(docs, 2)
        resolve (docs);
    })
})}

async function search( searchString ){ return new Promise((resolve, reject) => {
    consoleLogDebug("request to search societies, string: " + searchString, 1);
    getDb().collection('societies').find({ $text: {$search: searchString}},{ score:{ $meta: "textScore"}}).toArray(function (err, docs) {
        for (const doc of docs){ doc.entryClass = "societies" }
        resolve (docs);
    })
})}

async function searchRecent( maxNumber ) { return new Promise((resolve, reject) => {
    consoleLogDebug(`request to search the most recent ${maxNumber} societies`, 1);
    getDb().collection('societies').find().sort({"created_ts": -1}).limit(maxNumber).toArray(function (err, docs) {
        for (const doc of docs){ doc.entryClass = "societies" }
        consoleLogDebug(docs, 2)
        resolve (docs);
    })
})}

module.exports = { createSociety, getSocieties, getSocietyTypes, search, searchRecent }
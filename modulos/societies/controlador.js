const service = require('./servicio')
const {logger} = require('../../logger')

async function create(req, res){
    try{
        const society = req.body
        logger.info({ action: 'Request to create society' })
        entryId = await service.createSociety(society)
        logger.info({ action: 'Society created' })
        res.writeHead(201, 'Content-Type', 'application/json')
        res.end(JSON.stringify(entryId))
    } catch {
        logger.info({ action: 'Fail en society creation' })
        res.writeHead(500, 'Content-Type', 'application/json')
        res.end(JSON.stringify({ result:"Fail en society creation"}))
    }
}

async function getAll(req, res){

    logger.info({ action: 'request to get societies'})

    const societies = await service.getSocieties()
    console.log("controller: " + societies)
    
    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify(societies))
}

async function getTypes(req, res){

    logger.info({ action: 'request to get society types'})

    const societyTypeObjects = await service.getSocietyTypes()
    var societyTypes = []
    for (var piece of societyTypeObjects) {
      societyTypes.push( piece['name'] )
    }
    console.log("controller: " + societyTypes)
    
    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify(societyTypes))
}

async function search(req, res){

    logger.debug({ action: 'request to search society'})
    const searchString = req.body.searchString

    const objectsFound = await service.search(searchString)
    
    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify(objectsFound))
}

async function searchRecent(req, res){

    logger.debug({ action: 'request to search recent societies'})
    const maxNumber = req.body.maxNumber

    const objectsFound = await service.searchRecent(maxNumber)
    
    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify(objectsFound))
}

module.exports = { create, getAll, getTypes, search, searchRecent }